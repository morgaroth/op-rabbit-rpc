package io.morgaroth.oprabbit.examples

import com.spingo.op_rabbit.Directives._
import io.morgaroth.oprabbit.RabbitControlActor
import io.morgaroth.oprabbit.config.QueueConfig
import io.morgaroth.oprabbit.server.BaseRpcServer

object RabbitRpcServerExample {

  implicit val rc: RabbitControlActor = RabbitControlActor.deadLetters

  val queueCfg = QueueConfig("qname", "", 5, List("*"))

  val handleSimpleTaskHandler = routingKey {
    case "simple-task" =>
      println("simple task handled")
      ack()
  }

  val debugHandler =
    routingKey { rk =>
      body(as[String]) { content =>
        println(s"$rk: $content")
        ack()
      }
    }

  new BaseRpcServer(queueCfg, List(handleSimpleTaskHandler, debugHandler)).subscribe
}
