package io.morgaroth.oprabbit

import io.morgaroth.oprabbit.utilities.RpcJsonSupport
import org.json4s.DefaultFormats
import org.json4s.native.Serialization
import org.scalatest.{FlatSpec, Matchers}

case class SomeData(testValue: Int, someString: String)

class UnrecognizedExceptionTest extends FlatSpec with Matchers with RpcJsonSupport {

  implicit val f = DefaultFormats.withRpcSupport(
    Map(ErrorCodes.unrecognizedException -> classOf[UnrecognizedException])
  )

  def serializationLoop[T <: AnyRef : Manifest](value: T) = {
    val serialized = json.writePretty(value)
    try {
      json.read[T](serialized)
    } catch {
      case e: Throwable =>
        println(serialized)
        throw e
    }
  }

  val json = Serialization
  "UnrecognizedException class" should "be serialized to json with code value" in {
    val ex = new IllegalArgumentException("for testing purposes")
    val err = UnrecognizedException.from(ex)
    serializationLoop(err) shouldBe err
  }

  it should "work" in {

    val normal: Either[RPCError, SomeData] = Right(SomeData(111, "test data"))
    serializationLoop(normal) shouldBe normal

    val ex = new IllegalArgumentException("for testing purposes")
    val err: Either[RPCError, SomeData] = Left(UnrecognizedException.from(ex))
    serializationLoop(err) shouldBe err
  }
}
