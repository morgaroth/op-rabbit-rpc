package io.morgaroth.oprabbit.utilities

import com.typesafe.scalalogging.LazyLogging
import io.morgaroth.oprabbit.RPCError
import org.json4s.reflect.ScalaType
import org.json4s.{CustomSerializer, Extraction, FieldSerializer, Formats, JObject, JValue}

import scala.language.implicitConversions

object RpcErrorJsonFormats extends LazyLogging {

  def serializer(supportedErrorCodes: Set[Int]): PartialFunction[Any, JValue] = {
    {
      case error: RPCError if {
        if (!supportedErrorCodes(error.code)) {
          logger.error(s"UNREGISTERED IN JSON LIBRARY ERROR $error")
        }
        false // so below line will be newer executed, this allow serialization to be handled by generic methods
      } => ???
    }
  }

  def deserializer(classes: Map[Int, ScalaType], formats: Formats): PartialFunction[JValue, RPCError] = {
    case obj: JObject if obj.values.contains("code") =>
      val code = obj.values("code").asInstanceOf[Number].intValue()
      val cl = classes(code)
      val result = Extraction.extract(obj, cl)(formats).asInstanceOf[RPCError]
      assert(result.code == code)
      result
  }

  def apply(classes: Map[Int, Class[_ <: RPCError]]): CustomSerializer[RPCError] = {
    val res = classes.mapValues(ScalaType(_))
    new CustomSerializer[RPCError](formats => (deserializer(res, formats), serializer(classes.keySet)))
  }
}

object RpcErrorCodeFieldSerializer {
  val RpcErrorCodeFieldFormat: FieldSerializer[RPCError] = FieldSerializer[RPCError]()
}

trait RpcJsonSupport {
  implicit def wrapIt(f: Formats) = new {
    def withRpcSupport(errorCodesAndClasses: Map[Int, Class[_ <: RPCError]]): Formats = {
      f + RpcErrorJsonFormats(errorCodesAndClasses) + RpcErrorCodeFieldSerializer.RpcErrorCodeFieldFormat
    }
  }
}