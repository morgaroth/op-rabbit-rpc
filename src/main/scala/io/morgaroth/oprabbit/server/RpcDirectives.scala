package io.morgaroth.oprabbit.server

import com.spingo.op_rabbit.Directives.optionalProperty
import com.spingo.op_rabbit.properties.{CorrelationId, ReplyTo}


trait RpcDirectives {
  val rpcDirective = optionalProperty(ReplyTo).hflatMap(x => optionalProperty(CorrelationId).hmap {
    y => x.head.zip(y.head).headOption.map(RpcData.tupled)
  })
}

object RpcDirectives extends RpcDirectives