package io.morgaroth.oprabbit.server

import com.spingo.op_rabbit.Directives._
import com.spingo.op_rabbit._
import com.typesafe.scalalogging.LazyLogging
import io.morgaroth.oprabbit.RabbitControlActor
import io.morgaroth.oprabbit.config.QueueConfig

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Promise
import scala.language.implicitConversions
import scala.util.Try

trait BaseRpcServerLike extends LazyLogging {

  def subscribeQueue: QueueConfig

  def handlers: List[Handler]

  def subscribe()(implicit rc: RabbitControlActor, rs: RecoveryStrategy = RecoveryStrategy.none) = {
    Subscription.run(rc) {
      channel(qos = subscribeQueue.qos) {
        consume {
          topic(
            queue = queue(subscribeQueue.queueName, durable = subscribeQueue.durable, autoDelete = subscribeQueue.autoDelete),
            topics = subscribeQueue.routingList,
            exchange = subscribeQueue.getExchangeTopic
          )
        }(handler)
      }
    }
  }

  private def defaultHandler = (routingKey & body(as[String])) { case (rk, content) =>
    logger.warn(s"unhandled message $rk: $content")
    nack(false)
  }

  private def handler: Handler = (x: Promise[ReceiveResult], y: Delivery) => {
    val resultHandlers = handlers :+ defaultHandler

    def exec(rest: List[Handler]): Unit = {
      rest.headOption.map { h =>
        Try(h(x, y)).recover {
          case _: MatchError => exec(rest.tail)
          case e: Throwable =>
            logger.error("ERROR IN RPC ERROR HANDLED LOOP", e)
            e.printStackTrace()
          //          case err => respondWithHandlingError(err)(x, y)
        }
      }.get
    }

    exec(resultHandlers)
  }
}

class BaseRpcServer(val subscribeQueue: QueueConfig, val handlers: List[Handler]) extends BaseRpcServerLike