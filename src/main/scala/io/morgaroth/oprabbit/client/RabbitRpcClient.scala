package io.morgaroth.oprabbit.client

import java.util.UUID

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import akka.pattern.{AskTimeoutException, ask}
import akka.util.Timeout
import com.spingo.op_rabbit.Directives._
import com.spingo.op_rabbit._
import com.spingo.op_rabbit.properties.{CorrelationId, ReplyTo}
import com.typesafe.scalalogging.Logger
import io.morgaroth.oprabbit.config.ExchangeConfig
import io.morgaroth.oprabbit.{RPCError, RabbitControlActor}
import org.json4s.JValue
import org.json4s.JsonAST.JNull
import org.slf4j.LoggerFactory

import scala.collection.mutable
import scala.compat.Platform
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.language.postfixOps

class RabbitRpcClient(rpcQueue: ExchangeConfig)
                     (implicit val system: ActorSystem, val rc: RabbitControlActor, rs: RecoveryStrategy)
  extends RpcClient {

  val requestsLogger = Logger(LoggerFactory.getLogger("rpc.requests"))

  val queueName = s"rpc-${UUID.randomUUID.toString}"
  val subscription = Subscription.run(rc) {
    channel(qos = 3) {
      consume(queue(queueName, exclusive = true, autoDelete = true)) {
        property(CorrelationId) { correlationId =>
          extract(x => x) { msg =>
            val actor = system.actorSelection(s"/user/${actorFor(correlationId)}")
            actor ! RpcActor.MessageResponse(msg)
            ack
          }
        }
      }
    }
  }

  def forwardRPC[Request <: AnyRef, Response <: AnyRef](
                                                         requestName: String,
                                                         reqMap: Request => Request = (x: Request) => x)
                                                       (implicit m: RabbitMarshaller[Request], un: RabbitUnmarshaller[Response], errM: RabbitUnmarshaller[RPCError],
                                                        timeout: Timeout = 2.minutes): Request => Future[Response] =
    (req: Request) => doRPC[Request, Response](requestName, reqMap(req))

  def getRPC[Response <: AnyRef](requestName: String)
                                (implicit m: RabbitMarshaller[JValue], un: RabbitUnmarshaller[Response], errM: RabbitUnmarshaller[RPCError], timeout: Timeout) = {
    doRPC[JValue, Response](requestName, JNull)
  }

  def doRPC[Request <: AnyRef, Response <: AnyRef](
                                                    requestName: String,
                                                    req: Request,
                                                  )(implicit m: RabbitMarshaller[Request], un: RabbitUnmarshaller[Response], errM: RabbitUnmarshaller[RPCError], timeout: Timeout) = {
    doRawRPC[Request](requestName, req).map { x =>
      val charset = Option(x.properties.getContentEncoding)
      un.unmarshall(x.body, None, charset)
    }
  }

  def doRawRPC[Request <: AnyRef](requestName: String, req: Request)(implicit m: RabbitMarshaller[Request], timeout: Timeout) = {
    val correlationId = UUID.randomUUID.toString
    val actorName = actorFor(correlationId)
    val startTime = Platform.currentTime
    val actorRef = system.actorOf(Props(classOf[RpcActor], None), actorName)
    val result = (actorRef ? RpcActor.RpcRequest)
      .mapTo[Delivery]
      .map { resp =>
        val endTime = Platform.currentTime
        requestsLogger.info("RPC call: RK={}, entity={}, resp={}, took={}ms", requestName, req.toString.take(300), resp.toString.take(300), endTime - startTime)
        resp
      }.recover {
      case tm: AskTimeoutException =>
        requestsLogger.warn("RPC: timeout exception, it should never occur. If it occurs, change `responseTimeout` to bigger. DEBUG info: requestName {}, req {}", requestName, req)
        throw tm
    }.recover {
      case t: Throwable =>
        val endTime = Platform.currentTime
        requestsLogger.info("RPC call [error]: RK={}, entity={}, resp={}, took={}ms", requestName, req.toString.take(300), t.toString, endTime - startTime)
        throw t
    }
    sendMessage(requestName, correlationId, req)
    result
  }

  protected def sendMessage[T](routingKey: String, correlationId: String, message: T)(implicit marshaller: RabbitMarshaller[T]) = {
    val messageProperties = Seq(
      CorrelationId(correlationId),
      ReplyTo(queueName)
    )
    rc.actor ! Message.topic(message, routingKey, exchange = rpcQueue.exchangeName, properties = messageProperties)
  }

  protected def actorFor(messageId: String): String = s"rpc-$messageId"
}


trait CollectiveRequests {
  this: RabbitRpcClient =>

  def doCollectiveCall[Response <: AnyRef](requestName: String,
                                           responseTimeout: FiniteDuration
                                          )(implicit m: RabbitMarshaller[String], un: RabbitUnmarshaller[Response]) = {
    doCollectiveRPC[String, Response](requestName, "", responseTimeout)
  }

  def doCollectiveRPC[Request <: AnyRef, Response <: AnyRef](
                                                              requestName: String,
                                                              req: Request,
                                                              responseTimeout: FiniteDuration,
                                                            )(implicit m: RabbitMarshaller[Request], un: RabbitUnmarshaller[Response]) = {
    implicit val tm: Timeout = responseTimeout
    val correlationId = UUID.randomUUID.toString
    val actorName = actorFor(correlationId)
    val startTime = Platform.currentTime
    val actorRef = system.actorOf(Props(classOf[RpcActor], Some(responseTimeout)), actorName)
    val result = (actorRef ? RpcActor.RpcRequest)
      .mapTo[List[Delivery]]
      .map(_.map { x =>
        val charset = Option(x.properties.getContentEncoding)
        val contentType = Option(x.properties.getContentType)
        val data = new String(x.body, charset.getOrElse("utf-8"))
        val endTime = Platform.currentTime
        requestsLogger.info("Collective call: RK={}, entity={}, resp={}, took={}ms", requestName, req.toString.take(300), data.take(300), endTime - startTime)
        un.unmarshall(x.body, contentType, charset)
      }).recover {
      case tm: AskTimeoutException =>
        requestsLogger.warn("CollectiveCall: timeout exception, it should never occur. If it occurs, change `responseTimeout` to bigger. DEBUG info: requestName {}, req {}", requestName, req)
        throw tm
    }.recover {
      case t: Throwable =>
        val endTime = Platform.currentTime
        requestsLogger.info("Collective call [error]: RK={}, entity={}, resp={}, took={}ms", requestName, req.toString.take(300), t.toString, endTime - startTime)
        throw t
    }
    sendMessage(requestName, correlationId, req)
    result
  }

}


//@formatter:off
object RpcActor {
  case class MessageResponse(content: Delivery)
  case object RpcRequest
  case object TimeoutEnds
}
//@formatter:on

class RpcActor(timeout: Option[FiniteDuration]) extends Actor with ActorLogging {

  import RpcActor._

  val computationsLag = 200.millis // completely arbitrary value

  timeout.map { tm =>
    val timeLeft = List(tm, computationsLag).max - computationsLag
    context.system.scheduler.scheduleOnce(timeLeft, self, TimeoutEnds)
  }

  val singleResponse = timeout.isEmpty
  val acc = mutable.ListBuffer.empty[Delivery]
  var requester: Option[ActorRef] = None

  def receive = {
    case RpcRequest =>
      requester = Some(sender())
    case MessageResponse(content) if singleResponse =>
      requester.foreach(_ ! content)
      context stop self
    case MessageResponse(content) =>
      acc += content
    case TimeoutEnds =>
      requester.foreach(_ ! acc.toList)
      context stop self
  }
}