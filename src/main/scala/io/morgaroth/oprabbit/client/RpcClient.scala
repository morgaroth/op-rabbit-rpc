package io.morgaroth.oprabbit.client

import akka.util.Timeout
import com.spingo.op_rabbit.{RabbitMarshaller, RabbitUnmarshaller}
import io.morgaroth.oprabbit.RPCError

import scala.concurrent.Future

trait RpcClient {
  def doRPC[Request <: AnyRef, Response <: AnyRef](requestName: String, req: Request)
                                                  (implicit
                                                   m: RabbitMarshaller[Request],
                                                   un: RabbitUnmarshaller[Response],
                                                   errM: RabbitUnmarshaller[RPCError],
                                                   timeout: Timeout,
                                                  ): Future[Response]
}
