package io.morgaroth.oprabbit

import akka.actor.{Actor, ActorRef}

import scala.language.implicitConversions

case class RabbitControlActor(actor: ActorRef) {
  def !(message: Any)(implicit sender: ActorRef = Actor.noSender) {
    actor.tell(message, sender)
  }
}

object RabbitControlActor {
  val deadLetters = new RabbitControlActor(Actor.noSender)

  implicit def unwrapRabbitControlActor(wrapped: RabbitControlActor): ActorRef = wrapped.actor

  implicit def wrapRabbitControlActor(raw: ActorRef) = RabbitControlActor(raw)
}