package io.morgaroth.oprabbit.config

import com.typesafe.config.Config

import scala.util.Try

object ExchangeConfig {
  def fromConfig(config: Config) = {
    val exchangeName = config.getString("exchange-name")
    val messageTopic = Try(config.getString("message-topic")).getOrElse("")
    ExchangeConfig(exchangeName, messageTopic)
  }
}

case class ExchangeConfig(exchangeName: String, routingKey: String = "", isFanout: Boolean = false)